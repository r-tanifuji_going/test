package トランプ;

import java.util.ArrayList;
import java.util.Collections;

public abstract class Trump {
	private ArrayList<Integer> trump = new ArrayList<>(); //トランプの山
	private ArrayList<Integer> deck = new ArrayList<>(); //山札
	private ArrayList<Integer> discard = new ArrayList<>(); //捨て札

	protected ArrayList<Integer> layout = new ArrayList<>(); //場札

	protected ArrayList<ArrayList<Integer>> player = new ArrayList<>();//全プレイヤーの手札
	protected ArrayList<Integer> hand;//各プレイヤーの手札

	protected int turnCount = 1;//ターンのカウント
	protected int gameCount = 1;//ターンのカウント
	protected int turnPlayerFlag = 0;//ターンプレイヤーの数字

	//コンストラクタで山札を作成
	Trump() {
		for (int i = 0; i < 52; i++) {
			this.trump.add(i);
		}
	}

	//山札をシャッフル
	protected void shuffle() {
		Collections.shuffle(trump);
	}

	protected void reMakeTrump() {
		for (int i = 0; i < 52; i++) {
			this.trump.add(i);
		}
	}

	//数字からカードに変換
	protected String intToTrumpCard(int number) {
		String[] card = new String[2];

		String suit = "ジョーカー";
		switch (number / 13) {
		case 0:
			suit = "♠";
			break;
		case 1:
			suit = "♥";
			break;
		case 2:
			suit = "♦";
			break;
		case 3:
			suit = "♣";
			break;
		default:
			// 上記以外は、全てジョーカーとして扱う
		}
		card[0] = suit;
		card[1] = String.valueOf(number % 13 + 1);

		return card[0] + "-" + card[1];
	}

	//数字から絵札に変換
	protected String numToSuit(String num) {
		switch (num) {
		case "1":
			num = "A";
			break;
		case "11":
			num = "J";
			break;
		case "12":
			num = "Q";
			break;
		case "13":
			num = "K";
			break;
		default:
			break;
		}

		return num;
	}

	//以下GetterとSetter

	public ArrayList<Integer> getTrump() {
		return trump;
	}

	public void setTrump(ArrayList<Integer> trump) {
		this.trump = trump;
	}

	public ArrayList<Integer> getDeck() {
		return deck;
	}

	public void setDeck(ArrayList<Integer> deck) {
		this.deck = deck;
	}

	public ArrayList<Integer> getDiscard() {
		return discard;
	}

	public void setDiscard(ArrayList<Integer> discard) {
		this.discard = discard;
	}

	//抽象メソッド
	abstract void turnChange();//ターン移行処理
	abstract boolean cheakAllHands();//手札の確認処理
	abstract void sort(ArrayList<Integer> hands);//手札のソート処理
	abstract void deal(int playerNum);//手札を配る処理
	abstract void display(ArrayList<Integer> myhand, boolean flag);//手札表示処理
	abstract void display();//場表示処理
}
