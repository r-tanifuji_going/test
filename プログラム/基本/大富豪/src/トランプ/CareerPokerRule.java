package トランプ;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class CareerPokerRule extends Trump {
	Scanner scan = new Scanner(System.in);

	protected List<String> choiceNum = new ArrayList<String>();//入力された手札を示す数字
	protected ArrayList<Integer> finish = new ArrayList<>();//あがったプレイヤーの管理
	protected int finishBefore = -1;

	protected boolean[] passFlag;//パスしてるプレイヤーの管理

	//役の管理
	private boolean revolution = false;

	//手札が無くなった後のあがり表示
	protected void winner() {
		String name = "";
		if (turnPlayerFlag == 0) {
			name = "あなた";
		} else {
			name = "CPU" + turnPlayerFlag;
		}
		finish.add(turnPlayerFlag);
		System.out.println(name + "が" + finish.size() + "位抜けしました！");
		if(finish.size() == 1 && (turnPlayerFlag != finishBefore)) {

		}
		Util.sleep(3000);
		System.out.println();
		passFlag[turnPlayerFlag] = true;
	}

	//場にカードを置く
	protected void pull() {
		for (int i = 0; i < layout.size(); i++) {
			getDiscard().add(layout.get(i));
		}

		layout.clear();

		for (int i = 0; i < choiceNum.size(); i++) {
			//場にカードを置いて、手札からremove
			layout.add(player.get(turnPlayerFlag).get(Integer.parseInt(choiceNum.get(i)) - i));
			player.get(turnPlayerFlag).remove(Integer.parseInt(choiceNum.get(i)) - i);
		}

		if (layout.size() == 4) {
			System.out.println("革命！！！");
			Util.sleep(5000);
			if (!revolution) {
				revolution = true;
			} else {
				revolution = false;
			}
		}

		choiceNum.clear();
	}

	//CPUのカードを置く処理
	protected boolean pullCPU() {
		if (layout.size() == 0) {//何も無かったら
			int handSee = -1;
			int handFinish = 999;
			if (!revolution) {
				handSee = 0;
				handFinish = player.get(turnPlayerFlag).size();
			} else {
				handSee = player.get(turnPlayerFlag).size() - 1;
				handFinish = -1;
			}
			while (handSee != handFinish) {//手札の数繰り返し
				System.out.print("．");
				if (choiceNum.size() == 0) {
					//場に何も無かったらとりあえず一番弱い数を選択
					Util.sleep(150);
					addNum(handSee);
				} else {
					String[] handNum = intToTrumpCard(player.get(turnPlayerFlag).get(handSee)).split("-");
					int pullNum = Integer.parseInt(handNum[1]);

					int handChoiceNum = player.get(turnPlayerFlag).get(Integer.parseInt(choiceNum.get(0)));
					String[] handChoiceCard = intToTrumpCard(handChoiceNum).split("-");
					int pullChoiceNum = Integer.parseInt(handChoiceCard[1]);

					//System.out.println("同数チェックCPU : " + pullChoiceNum + "=" + pullNum);
					//現在選択している数と同じだったら一緒に選択。違うならそのまま出す。
					if (pullChoiceNum == pullNum) {
						addNum(handSee);
					} else {
						System.out.println(choiceNum + "上");
						System.out.println("");
						return true;
					}
				}

				if (!revolution) {
					handSee++;
				} else {
					handSee--;
				}
			}
			System.out.println("");
			return true;
		} else {
			String[] layoutNum = intToTrumpCard(layout.get(0)).split("-");
			int seeNum = Integer.parseInt(layoutNum[1]);

			for (int i = 0; i < player.get(turnPlayerFlag).size(); i++) {//手札の数繰り返し
				//System.out.print(i + ",");
				System.out.print("．");
				String[] handNum = intToTrumpCard(player.get(turnPlayerFlag).get(i)).split("-");
				int pullNum = Integer.parseInt(handNum[1]);

				if (choiceNum.size() == 0) {//まだ選択してないなら
					Util.sleep(200);
					//1と2は比較時＋１３して処理
					if (seeNum == 1 || seeNum == 2) {
						seeNum += 13;
					}
					if (pullNum == 1 || pullNum == 2) {
						pullNum += 13;
					}

					//場の数と今見てる数を比べて大きいなら選択
					if (!revolution) {
						if (seeNum < pullNum) {
							addNum(i);
						}
					} else {
						if (seeNum > pullNum) {
							addNum(i);
						}
					}
				} else {//既に一枚以上選択してるなら
					//System.out.println(choiceNum.get(0));
					int handChoiceNum = player.get(turnPlayerFlag).get(Integer.parseInt(choiceNum.get(0)));
					String[] handChoiceCard = intToTrumpCard(handChoiceNum).split("-");
					int pullChoiceNum = Integer.parseInt(handChoiceCard[1]);

					//現在選択している数と同じだったら一緒に選択。
					if (pullChoiceNum == pullNum) {
						addNum(i);
					} else {//もし違うなら場の枚数と異なるので選択をすべてリセットして次を見る
						choiceNum.clear();
						addNum(i);
					}
				}

				//現在、選択してる枚数と場の枚数が同じなら出す
				if (layout.size() == choiceNum.size()) {
					System.out.println(choiceNum + "下");
					System.out.println("");
					return true;
				}
				//場の枚数に足りないなら次を見る
			}
			System.out.println("");
			return false;
		}
	}

	//プレイヤーの選択したカードが出せるかチェックする
	protected boolean cheakPull() {
		//場:layout
		//手札:hand
		//指定数字：choiceNum

		if (layout.size() == 0) {//何も無かったら置ける
			return true;
		}

		if (layout.size() != choiceNum.size()) {//枚数のチェック
			//System.out.println("枚数");
			return false;
		}

		String[] layoutStr = intToTrumpCard(layout.get(0)).split("-");
		int handChoiceStr = Integer.parseInt(choiceNum.get(0));
		String[] handStr = intToTrumpCard(player.get(turnPlayerFlag).get(handChoiceStr)).split("-");
		int layoutNum = Integer.parseInt(layoutStr[1]);
		int handNum = Integer.parseInt(handStr[1]);

		//1と2は比較時＋１３して処理
		if (layoutNum == 1 || layoutNum == 2) {
			layoutNum += 13;
		}
		if (handNum == 1 || handNum == 2) {
			handNum += 13;
		}

		if (!revolution) {
			if (layoutNum > handNum) {//数字のチェック
				//System.out.println("数字 : " + layoutNum + "<" + handNum);
				return false;
			}
		} else {
			if (layoutNum < handNum) {
				return false;
			}
		}

		return true;
	}

	//プレイヤーの入力した値が正しい値かチェック
	protected boolean cheakPull(String line, boolean flag) {
		choiceNum = Util.splitAt(2, line);
		//System.out.println(choiceNum);
		for (int i = 0; i < choiceNum.size(); i++) {//半角2文字ごとに区切れないならfalse
			if (choiceNum.get(i).length() != 2) {
				System.out.println("半角数字2文字で入力してください");
				return false;
			}
		}
		if (!line.matches("[0-9]+")) {//数字以外が入力されたらfalse
			System.out.println("数字で入力してください");
			return false;
		}
		for (int i = 0; i < choiceNum.size(); i++) {//入力した数字が手札に合わないならfalse
			if (Integer.parseInt(choiceNum.get(i)) < player.get(turnPlayerFlag).size()) {
			} else {
				System.out.println("自分の手札の中の札を選択してください");
				return false;
			}
		}
		//TODO ここは階段処理を作る時には別処理に
		if (flag) {
			for (int i = 0; i < choiceNum.size() - 1; i++) {//離れた数字ならfalse
				int now = Integer.parseInt(choiceNum.get(i));
				int next = Integer.parseInt(choiceNum.get(i + 1));
				if (now + 1 != next) {
					System.out.println("続けた数字で入力してください");
					return false;
				}
			}

			for (int i = 0; i < choiceNum.size() - 1; i++) {//複数選択された場合に異なる数字ならfalse
				int nowNum = Integer.parseInt(choiceNum.get(i));
				int nextNum = Integer.parseInt(choiceNum.get(i + 1));
				String[] now = intToTrumpCard(player.get(turnPlayerFlag).get(nowNum)).split("-");
				String[] next = intToTrumpCard(player.get(turnPlayerFlag).get(nextNum)).split("-");
				if (Integer.parseInt(now[1]) != Integer.parseInt(next[1])) {
					System.out.println("同じカードを選択してください");
					return false;
				}
			}
		}
		return true;
	}

	//ターンの開始時に他プレイヤーのパス状況を見て、全員パスなら場をリセット
	protected boolean cheakPass() {
		int count = 0;
		for (int i = 0; i < player.size(); i++) {
			if (passFlag[i] == true) {
				count++;
			}
		}
		if (count == 3) {
			return true;
		} else {
			return false;
		}
	}

	//全プレイヤーの手札を見てスペードの3を持っているプレイヤーを先手プレイヤーに
	protected int cheakSpeedThree(ArrayList<ArrayList<Integer>> player) {
		for (int i = 0; i < player.size(); i++) {
			for (int j = 0; j < player.get(i).size(); j++) {
				if (player.get(i).get(j) == 2) {
					return i;
				}
			}
		}
		System.out.println("♠3が見つかりません");
		System.out.println("強制終了します");
		System.exit(-1);
		return 0;
	}

	//入力を半角2文字で管理しているため1文字の場合は0をつける
	private void addNum(int num) {
		String numStr = String.valueOf(num);
		if (numStr.length() == 1) {
			numStr = "0" + numStr;
		}
		choiceNum.add(numStr);
	}

	private void change(String numStr, int num) {
		int pull = 0;;
		int[] pullInt = new int[2];

		//初期値の設定//0が強→弱、1が弱→強
		pullInt[0] = 0;
		pullInt[1] = 0;

		switch (num) {
		case 2:
			List<String> list = Util.splitAt(2, numStr);
			for (int i = 0; i < pullInt.length; i++) {
				pullInt[i] = Integer.parseInt(list.get(i));
			}
			break;
		case 1:
			pull = Integer.parseInt(numStr);
			break;
		default:
			break;
		}

		//1位と4位の交換
		for (int i = 0; i < 2; i++) {
			player.get(finish.get(0)).add(player.get(finish.get(3)).get(player.get(finish.get(3)).size() - 1 - i));
			player.get(finish.get(3)).remove(player.get(finish.get(3)).size() - 1 - i);
			player.get(finish.get(3)).add(player.get(finish.get(0)).get(pullInt[i] - i));
			player.get(finish.get(0)).remove(pullInt[i] - i);
		}

		//2位と3位の交換
		player.get(finish.get(1)).add(player.get(finish.get(2)).get(player.get(finish.get(2)).size() - 1));
		player.get(finish.get(2)).remove(player.get(finish.get(2)).size() - 1);
		player.get(finish.get(2)).add(player.get(finish.get(1)).get(pull));
		player.get(finish.get(1)).remove(pull);
	}

	protected void handChange() {
		int rank = -1;
		String take = "";
		boolean numFlag = false;

		for (int i = 0; i < finish.size(); i++) {
			if (finish.get(i) == 0) {
				rank = i + 1;
				break;
			}
		}

		switch (rank) {
		case 1:
			display(player.get(0), true);
			numFlag = cheakPull("", false);
			while (!numFlag) {
				try {
					take = scan.next();
				} catch (NoSuchElementException nsee) {
					System.out.println("想定外のエラー(NSEE)");
					System.out.println("プログラムを終了します");
					System.exit(-1);
					nsee.printStackTrace();
				} catch (IllegalStateException ise) {
					ise.printStackTrace();
					System.out.println("想定外のエラー(ISE)");
					System.out.println("プログラムを終了します");
					System.exit(-1);
				}
				numFlag = cheakPull(take, false);
			}
			change(take, 2);
			break;
		case 2:
			display(player.get(0), true);
			numFlag = cheakPull("", false);
			while (!numFlag) {
				try {
					take = scan.next();
				} catch (NoSuchElementException nsee) {
					System.out.println("想定外のエラー(NSEE)");
					System.out.println("プログラムを終了します");
					System.exit(-1);
					nsee.printStackTrace();
				} catch (IllegalStateException ise) {
					ise.printStackTrace();
					System.out.println("想定外のエラー(ISE)");
					System.out.println("プログラムを終了します");
					System.exit(-1);
				}
				numFlag = cheakPull(take, false);
			}
			change(take, 1);
			break;
		case 3:
			change(take, -1);
			break;
		case 4:
			change(take, -2);
			break;
		default:
			break;
		}
	}

	protected void flagReset() {
		revolution = false;
	}

	@Override
	protected void turnChange() {//ターン移行処理、プレイヤー数を超えるならループするように
		turnPlayerFlag++;
		if (turnPlayerFlag > player.size() - 1) {
			turnPlayerFlag -= 4;
		}
	}

	@Override
	protected boolean cheakAllHands() {//全員の手札を見て手札があるのが1人だけならゲーム終了
		int count = 0;
		for (int i = 0; i < player.size(); i++) {//1枚でもある人の数をカウント
			if (player.get(i).size() != 0) {
				count++;
			}
		}

		if (count > 1) {//2人以上ならまだ続く
			return true;
		} else {
			for (int i = 0; i < player.size(); i++) {
				boolean flag = true;
				for (int j = 0; j < finish.size(); j++) {
					if (finish.get(j) == i) {
						flag = false;
						break;
					}
				}
				if (flag == true) {
					//ゲーム終了処理
					//ただし、残っている手札を捨て札にする
					finish.add(i);
					while (player.get(i).size() != 0) {
						getDiscard().add(player.get(i).get(0));
						player.get(i).remove(0);
					}
					for (int j = 0; j < layout.size(); j++) {
						getDiscard().add(layout.get(j));
					}
					break;
				}
			}
			return false;
		}
	}



	@Override
	protected void sort(ArrayList<Integer> hands) {
		for (int i = 0; i < hands.size() - 1; i++) {
			for (int j = hands.size() - 1; j > i; j--) {
				int left = hands.get(j - 1) % 13;
				int right = hands.get(j) % 13;
				if (left < 2) {
					left += 13;
				}
				if (right < 2) {
					right += 13;
				}
				if (left > right) {
					int tmpNum = hands.get(j - 1);
					hands.set(j - 1, hands.get(j));
					hands.set(j, tmpNum);
				}
			}
		}
	}

	@Override
	protected void deal(int playerNum) {//カードを配る
		shuffle(); //トランプをシャッフル
		//今回は山札はないので山札変数は使いません
		while (getTrump().size() != 0) {//山札が無くなるまで
			//for (int i = 0; i < getTrump().size(); i++) { //トランプを全て
			for (int j = 0; j < playerNum; j++) { //プレイヤーごとに
				if (getTrump().size() % playerNum == j) { //余剰でプレイヤーを判断
					player.get(j).add(getTrump().get(0)); //1枚ずつ各プレイヤーに配る
					getTrump().remove(0);
					if (j == 0) { //もし配ったのがプレイヤーなら
						display(player.get(0), false); //手札を表示
						sort(player.get(0)); //その後ソート
					}
				}
			}
		}
	}

	@Override
	protected void display(ArrayList<Integer> myhand, boolean flag) {
		System.out.print("_");
		for (int i = 0; i < myhand.size(); i++) {
			System.out.print("___"); //上線
		}
		System.out.println();
		for (int i = 0; i < myhand.size(); i++) {
			System.out.print("|");
			String[] card = intToTrumpCard(myhand.get(i)).split("-");
			System.out.print(card[0]); //スート表示
			System.out.print(" "); //空白表示
		}
		System.out.print("|");
		System.out.println();
		for (int i = 0; i < myhand.size(); i++) {
			System.out.print("|");
			String[] card = intToTrumpCard(myhand.get(i)).split("-");
			if (numToSuit(card[1]).length() == 1) {
				card[1] = " " + numToSuit(card[1]);
			} //空白挿入
			System.out.print(card[1]); //数字表示
		}
		System.out.print("|");
		System.out.println();
		for (int i = 0; i < myhand.size(); i++) {
			System.out.print("~~~"); //下線
		}
		System.out.print("~");
		System.out.println();
		if (!flag) {
			Util.sleep(500);
		} else {
			for (int i = 0; i < myhand.size(); i++) {
				System.out.print("|");
				String handNum = String.valueOf(i);
				if (handNum.length() == 1) {
					handNum = "0" + handNum;
				} //空白挿入
				System.out.print(handNum); //数字表示
			}
			System.out.println("|");
		}
	}

	@Override
	protected void display() {
		if (layout.size() != 0) {
			System.out.print("_");
			for (int i = 0; i < layout.size(); i++) {
				System.out.print("___"); //上線
			}
			System.out.println();
			for (int i = 0; i < layout.size(); i++) {
				System.out.print("|");
				String[] card = intToTrumpCard(layout.get(i)).split("-");
				System.out.print(card[0]); //スート表示
				System.out.print(" "); //空白表示
			}
			System.out.print("|");
			System.out.println();
			for (int i = 0; i < layout.size(); i++) {
				System.out.print("|");
				String[] card = intToTrumpCard(layout.get(i)).split("-");
				if (numToSuit(card[1]).length() == 1) {
					card[1] = " " + numToSuit(card[1]);
				} //空白挿入
				System.out.print(card[1]); //数字表示
			}
			System.out.print("|");
			System.out.println();
			for (int i = 0; i < layout.size(); i++) {
				System.out.print("~~~"); //下線
			}
			System.out.print("~");
			System.out.println();
		} else {
			System.out.println("____"); //上線
			System.out.println("|　|");
			System.out.println("|　|");
			System.out.println("~~~~"); //下線
		}
	}
}
