package 大富豪;

import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.Scanner;

import トランプ.CareerPokerRule;

public class Start extends CareerPokerRule {
	Scanner scan = new Scanner(System.in);

	private boolean turnFlag = false;//ターンの終了条件

	String pullCardNum;

	private boolean turn() {
		if (!cheakAllHands()) { //ゲームの終了判定
			System.out.println("勝敗が決定しました。");
			return true;
		}
		if (passFlag[turnPlayerFlag] == true) {
			return false;
		}
		if (cheakPass()) {
			for (int i = 0; i < layout.size(); i++) {
				getDiscard().add(layout.get(i));
			}
			layout.clear();
			display();
			for (int i = 0; i < player.size(); i++) {
				if (player.get(i).size() != 0) {
					passFlag[i] = false;
				}
			}
		}

		System.out.println(turnCount + "ターン目");//ターン数の表示
		turnCount++;

		if (turnPlayerFlag == 0) {
			System.out.println("あなたのターンです。");
		} else {
			System.out.println("CPU" + turnPlayerFlag + "のターンです。");
		}

		turnFlag = false;
		while (!turnFlag) {
			if (turnPlayerFlag == 0) {
				//display();
				System.out.println("場に出すカードを入力してください");
				System.out.println("パスの場合は\"pass\"または\"-1\"と入れてください");
				display(player.get(turnPlayerFlag), true); //入力用数字を

				try {
					pullCardNum = scan.next();
				} catch (NoSuchElementException nsee) {
					System.out.println("想定外のエラー(NSEE)");
					System.out.println("プログラムを終了します");
					System.exit(-1);
					nsee.printStackTrace();
				} catch (IllegalStateException ise) {
					ise.printStackTrace();
					System.out.println("想定外のエラー(ISE)");
					System.out.println("プログラムを終了します");
					System.exit(-1);
				}

				if (pullCardNum.equals("9999")) {
					System.out.println("特殊処理");
					for (int i = 0; i < player.size(); i++) {
						while(player.get(i).size() != 1) {
							player.get(i).remove(0);
						}
					}
					continue;
				}

				if (pullCardNum.equals("pass") || pullCardNum.equals("-1")) {
					System.out.println("パスします\r\n");
					passFlag[turnPlayerFlag] = true;
					choiceNum.clear();
					break;
				}
				if (!cheakPull(pullCardNum, true)) {
					choiceNum.clear();
					continue;
				}

				if (!cheakPull()) {
					System.out.println("そのカードは置けません");
					choiceNum.clear();
					continue;
				}

				pull();
				display();
				turnFlag = true;
			} else {
				//display(player.get(turnPlayerFlag), true); //入力用数字を
				System.out.print("思考中");
				if (!pullCPU()) {
					System.out.println("パスします\r\n");
					passFlag[turnPlayerFlag] = true;
					choiceNum.clear();
					break;
				}
				pull();
				display();
				turnFlag = true;
			}
		}

		if (player.get(turnPlayerFlag).size() == 0) {
			winner();
		}

		return false;
	}

	protected void Start(int playerNum) {
		for (int i = 0; i < playerNum; i++) {
			hand = new ArrayList<>(); //プレイヤー人数分のArrayListを作成
			player.add(hand); //playerListに代入
		}

		while (true) {
			System.out.println(gameCount + "回目のゲームを開始");
			passFlag = new boolean[playerNum];

			//プレイヤーに1枚ずつトランプを配る
			deal(playerNum);

			display(player.get(0), false); //最終的な手札を表示

			for (int i = 1; i < playerNum; i++) {
				sort(player.get(i)); //手札をソート
			}

			if (gameCount > 1) {
				System.out.println("手札を交換します");
				handChange();
				for (int i = 0; i < playerNum; i++) {
					sort(player.get(i)); //手札をソート
				}
				turnPlayerFlag = finish.get(0); //とりあえずターンプレイヤーをプレイヤーに
			} else {
				turnPlayerFlag = cheakSpeedThree(player); //スペードの3を持ってるプレイヤーをターンプレイヤーに
			}

			for (int i = 0; i < passFlag.length; i++) {
				display(player.get(i), true);
			}

			finish.clear();
			System.out.println("ゲームを開始します。");
			display();
			while (!turn()) {
				//display(player.get(0), false);
				turnChange();
			}

			//勝敗の処理
			System.out.println("結果は以下の通りです\r\n");
			for (int i = 0; i < player.size(); i++) {
				String name = "";
				if (finish.get(i) == 0) {
					name = "あなた";
				} else {
					name = "CPU" + finish.get(i);
				}
				System.out.println("" + (i + 1) + "位 : " + name);
			}
			System.out.println("もう一度遊びますか？？？");
			System.out.println("遊ぶならYを選択。それ以外なら終了します");
			String next = scan.next();

			if (!next.matches("[Yy]")) {
				System.out.println("大富豪を終了します");
				System.exit(1);
			}

			gameCount++;
			turnCount = 1;
			getDiscard().clear();
			reMakeTrump();
			layout.clear();
			choiceNum.clear();
			flagReset();
		}
	}
}
