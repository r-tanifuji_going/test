import java.io.InputStreamReader;
import java.io.FileWriter;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

public class Turn {
	String className;																					//エラー用
	String methodName = Util.getMethodName();															//エラー用
																										//
	BufferedReader brStone = new BufferedReader(new InputStreamReader(System.in));						//CMDからの読み込み用
	private BufferedWriter bwLog;																		//ログ書き込み用
																										//
	private boolean cheakTurn = false;																	//ターン終了条件false
	private boolean cheakPoint = false;																	//石の指定false
	private boolean cheakPut = false;																	//石が置けるかfalse
																										//
	private String logName;																				//ログファイル名格納用
	private int[][] bord = new int[10][10];																//ターン毎のボード格納用
	private int[] nowTurn;																				//現在ターンのターンプレイヤー情報
	private int turnCount;																				//現在の経過ターン数
	private String isPlayer;																			//先攻格納用
	private String turnPlayer;																			//ターンプレイヤー格納用
	private String turnColor;																			//ターンプレイヤーのカラー情報
	private String putPoint;																			//石の指定位置格納用
	private int line = -1, row = -1;																	//石の指定位置格納用
	private String[] putPointSplit;																		//石の指定位置格納用
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	private boolean turnStorn(int line, int row, int lineChange, int rowChange, boolean isTurn) {		//
		try {																							//
			if (bord[line][row] == 2) {																	//2個目が壁だったら即false
				return false;																			//
			} else if (bord[line][row] == nowTurn[0]) {													//2個以降に自分の石があったら裏返し処理開始
				return true;																			//
			} else {																					//
				if (turnStorn(line + lineChange, row + rowChange, lineChange, rowChange, isTurn)) {		//指定地から参照方向へ石の参照
					if (isTurn) {																		//
						bord[line][row] *= -1;															//反転フラグがtrueなら石を裏返す
					}																					//
					return true;																		//裏返し処理完了true
				} else {																				//
					return false;																		//裏返し処理を行わないfalse
				}																						//
			}																							//
		} catch (ArrayIndexOutOfBoundsException aie) {													//頼もしいエラー処理達
			System.out.println("line : " + line);														//
			System.out.println("row : " + row);															//
			System.out.println("lineChange : " + lineChange);											//
			System.out.println("rowChange : " + rowChange);												//
			System.out.println("isTurn : " + isTurn);													//
			Util.close(bwLog);																				//
			System.exit(1);																				//これらが発生したら壁の外にいってるっぽい
			return false;																				//
		}																								//
	}																									//
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	private boolean returnCheak(int line, int row, int lineChange, int rowChange, boolean isTurn) {		//
		try {																							//
			if (bord[line][row] != nowTurn[0] * -1) {													//相手の石以外がおいてある
				return false;																			//
			} else {																					//
				if (turnStorn(line + lineChange, row + rowChange, lineChange, rowChange, isTurn)) {		//指定地から参照方向へ石の参照
					if (isTurn) {																		//
						bord[line][row] *= -1;															//反転フラグがtrueなら石を裏返す
					}																					//
					return true;																		//裏返し処理完了true
				} else {																				//
					return false;																		//裏返し処理を行わないfalse
				}																						//
			}																							//
		} catch (ArrayIndexOutOfBoundsException aie) {													//頼もしいエラー処理達
			System.out.println("line : " + line);														//
			System.out.println("row : " + row);															//
			System.out.println("lineChange : " + lineChange);											//
			System.out.println("rowChange : " + rowChange);												//
			System.out.println("isTurn : " + isTurn);													//
			Util.close(bwLog);																				//
			System.exit(1);																				//これらが発生したら壁の外にいってるっぽい
			return false;																				//
		}																								//
	}																									//
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	private void putStone() {																			//実際に石を置く処理
		bord[line][row] = nowTurn[0];																	//指定位置に石を置く
																										//
		for(int i = line - 1; i < line + 2; i++) {														//前八方向の裏返し処理
			for (int j = row - 1; j < row + 2; j++) {													//
				if (returnCheak(i, j, i - line, j - row, true)) {										// i 		= 現在の行位置
				} else {																				//
				}																						//
			}																							// j 		= 現在の列位置
		}																								// i - line = 指定位置からの行方向
																										//
		try {																							//
			String turnCountStr = String.valueOf(turnCount);											//ターン数をString型に変換
			if (turnCountStr.length() == 1) {turnCountStr = "0" + turnCountStr;}						//0~9なら0を追加
			bwLog.write(turnCountStr + "ターン目 : " + turnColor + " : " + line + "-" + row);			//
			bwLog.newLine();																			//
			bwLog.flush();																				//
		} catch (IOException ie) {																		//
			ie.printStackTrace();																		//
			System.out.println("パスの時に入力エラー");													//
			Util.close(bwLog);																				//
			System.exit(-1);																			//
		}																								//
																										// j - row	= 指定位置かたの列方向
		cheakTurn = true;																				//
		cheakPoint = false;																				//
		cheakPut = false;																				//ターン終了条件をtrueに
	}																									//他の処理を停止するためにfalseに
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	private boolean cheakPutStone() {																	//石が置けるかどうかの判定
		className = Util.getClassName();																//エラー用
																										//
		for(int i = line - 1; i < line + 2; i++) {														//
			for (int j = row - 1; j < row + 2; j++) {													//
				if (bord[line][row] == 0 && returnCheak(i, j, i - line, j - row, false)) {				//石の配置場所に壁または石があればfalse
					return true;																		//石を置いても石が返らないならfalse
				} else {																				//
				}																						//
			}																							//
		}																								//
																										//
		System.out.println("その場所には石を置けません");												//
		cheakPoint = false;																				//
		return false;																					//
	}																									//
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	private boolean cheakPut(int lineCPU, int rowCPU) {													//
		className = Util.getClassName();																//エラー用
																										//
		for(int i = line - 1; i < line + 2; i++) {														//
			for (int j = row - 1; j < row + 2; j++) {													//
				if (bord[line][row] == 0 && returnCheak(i, j, i - line, j - row, false)) {				//石の配置場所に壁または石があればfalse
					return true;																		//石を置いても石が返らないならfalse
				} else {																				//
				}																						//
			}																							//
		}																								//
		return false;																					//
	}																									//
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	public boolean putStonePointSetCPU() {																//CPUが石置けるか
		ArrayList<Integer> lineRandom = new ArrayList<Integer>();										//ランダム用行リスト
		ArrayList<Integer> rowRandom = new ArrayList<Integer>();										//ランダム用列リスト
																										//
		for(int i = 1; i < 9; i++) {																	//
			lineRandom.add(i);																			//リストに初期値を代入
			rowRandom.add(i);																			//
		}																								//
																										//
		Collections.shuffle(lineRandom);																//行リストをシャッフル
		Collections.shuffle(rowRandom);																	//列リストをシャッフル
																										//
		System.out.print("思考中");																		//思考中の遅延演出
		for (int i = 0; i < 3; i++) {																	//
			System.out.print("．");																		//
			try {																						//
				Thread.sleep(500);																		//0.5秒の遅延で思考を演出
			} catch (InterruptedException e) {															//
				System.out.println("Sleepでエラー");													//
				return false;																			//
			}																							//
		}																								//
		System.out.print("\r\n");																		//
		try {																							//
			for(int i = 0; i < 8; i++) {																//ランダムに格納したArrayListを順に解決
				for (int j = 0; j < 8; j++) {															//
					line = lineRandom.get(i);															//配置場所を格納
					row = rowRandom.get(j);																//
					if (bord[line][row] == 0 && cheakPut(line, row)) {									//石の配置場所に壁または石があればfalse
						return true;																	//石を置いても石が返らないならfalse
					} else {																			//
					}																					//
				}																						//
			}																							//
		} catch (IndexOutOfBoundsException ioe) {														//
			ioe.printStackTrace();																		//
			System.out.println("line : " + line);														//
			System.out.println("row : " + row);															//
		}																								//
		System.out.println("想定外のエラー");															//想定外のエラー
		System.out.println("CPU起動してるのに置ける場所が見つかりません");								//
		System.out.println("プログラム製作者に連絡してください");										//
		Util.close(bwLog);																					//
		System.exit(-1);																				//強制終了
		return false;																					//
	}																									//
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	private boolean putStonePointSet() {																//石を置く位置の入力メソッド
		System.out.print("石を置く場所を選択してください(横-縦): ");									//
		try {																							//
			putPoint = brStone.readLine();																//石を置く場所を取得
		} catch (NullPointerException nfe) {															//
			System.out.println("石を置く場所を入力して下さい");											//
			return false;																				//
		} catch (IOException ie) {																		//これが出たら入力機器のエラー
			System.out.println("入力エラー");															//
			return false;																				//
		}																								//
																										//
		putPointSplit = putPoint.split("-");															//'-'でスプリット
		if(putPointSplit.length != 2) {																	//数値格納配列が2以外だったらエラー
			System.out.println("1-1 という形で入力して下さい");											//
			return false;																				//
		}																								//
																										//
		for (int i = 0; i < 2; i++) {																	//全角が入力されていたらエラー
			if (putPointSplit[i].getBytes().length > 1){												//
				System.out.println("半角で入力して下さい");												//
				return false;																			//
			}																							//
		}																								//
																										//
		line = Integer.parseInt(putPointSplit[0]);														//配置場所を格納
		row = Integer.parseInt(putPointSplit[1]);														//
																										//
		if (line < 0 || row < 0) {																		//数字が1−8以外であればエラー
			System.out.println("1〜9の値を入力してください");											//10は全角処理で排除されるため不要
			return false;																				//
		}																								//
																										//
																										//
		return true;																					//
	}																									//
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	private boolean chaekPutStoneNum() {																//
		for(int i = 1; i < 9; i++) {																	//
			for (int j = 1; j < 9; j++) {																//
				line = i;																				//配置場所を格納
				row = j;																				//
				if (bord[line][row] == 0 && cheakPut(line, row)) {										//石の配置場所に壁または石があればfalse
					return true;																		//石を置いても石が返らないならfalse
				} else {																				//
				}																						
			}																							//
		}																								//
		return false;																					//
	}																									//
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	private String cheakTurnPlayer(int count) {															//現在のターンプレイヤーを取得
		if ((isPlayer.equals("貴方") && (count % 2 == 1)) ||											//
			(isPlayer.equals("相手") && (count % 2 == 0))) {											//先攻が"貴方"かつ奇数ターン
			return "貴方";																				//先攻が"相手"かつ偶数ターン
		} else {																						//
			return "相手";																				//なら貴方がターンプレイヤー
		}																								//違うなら相手がターンプレイヤー
	}																									//
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	private boolean finishFlag() {																		//
		for(int j = 1; j < 9; j++) {																	//引数と同じ石の数をカウント
			for(int k = 1; k < 9; k++) {																//
				if (bord[j][k] == 0) {																	//
					return false;																		//
				}																						//
			}																							//
		}																								//
		return true;																					//
	}																									//
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	protected boolean Turn(int[][] bord, int[] nowTurn, int turnCount, String logName) {				//
		this.bord = bord;																				//現在のゲーム盤情報をセット
		this.nowTurn = nowTurn;																			//現在のターンプレイヤー情報をセット
		this.turnCount = turnCount;																		//現在のターン数情報をセット
		this.logName = logName;																			//ログファイル名をセット
		cheakTurn = false;																				//ターンの終了条件をfalse
																										//
		if (finishFlag()) {																				//ゲーム終了条件を満たすならfalseを返す
			return false;																				//
		}																								//
																										//
		try {																							//
			bwLog = new BufferedWriter(new FileWriter("GAME_log/" + logName + ".log", true));			//ログファイル書き込み用
		} catch (IOException ie) {																		//
			ie.printStackTrace();																		//
			System.out.println("入力エラー");															//
		}																								//
		if (nowTurn[0] == 1) {																			//ターンプレイヤーが1なら黒
			turnColor = "●";																			//
		} else {																						//ターンプレイヤーが0なら白
			turnColor = "○";																			//
		}																								//
																										//
		if (turnCount == 1) {																			//1ターン目のプレイヤー情報比較
			if (nowTurn[0] == 1) {isPlayer = "貴方";}													//違うなら相手
			else {isPlayer = "相手";}																	//
		}																								//2ターン目以降は判定しない
																										//
		System.out.print("ターン" + turnCount +" : ");													//ターン数の表示
		turnPlayer = cheakTurnPlayer(turnCount);														//ターンプレイヤーの確認
		System.out.println(turnPlayer + "のターンです。");												//
																										//
		while (!cheakTurn) {																			//ターン終了条件がTRUEになるまで繰り返し
			if (chaekPutStoneNum()) {																	//石がそもそも置けるかの判定
				if (turnPlayer.equals("貴方")) {														//相手か自分で石の指定方法を変更
					cheakPoint = putStonePointSet();													//石を置く位置の入力の判定
				} else {																				//
					cheakPoint = putStonePointSetCPU();													//CPUによる石の選択
				}																						//
				while(cheakPoint) {																		//石を置く位置の入力が正しくなるまで繰り返し
					cheakPut = cheakPutStone();															//石が置けるかどうかの判定
					while (cheakPut) {																	//石が正しく置けるまで繰り返し
						putStone();																		//
					}																					//
				}																						//
			} else {																					//
				System.out.println("石を置ける場所がありません");										//石が置ける場所が0ならそのままターン移行
				System.out.println("ターンを移行します");												//
				try {																					//パスターンでも棋譜を書き込み
					String turnCountStr = String.valueOf(turnCount);									//ターン数をString型に変換
					if (turnCountStr.length() == 1) {turnCountStr = "0" + turnCountStr;}				//0~9なら0を追加
					bwLog.write(turnCountStr + "ターン目 : " + turnColor + ":パス");					//
					bwLog.newLine();																	//
					bwLog.flush();																		//
				} catch (IOException ie) {																//
					ie.printStackTrace();																//
					System.out.println("パスの時に入力エラー");											//
					Util.close(bwLog);																	//
					System.exit(-1);																	//強制終了
				}																						//
				cheakTurn = true;																		//
			}																							//
		}																								//
		Util.close(bwLog);																				//
		return true;																					//ターン終了
	}																									//
}																										//
