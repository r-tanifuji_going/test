import java.io.File;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Calendar;
import java.text.SimpleDateFormat;

public class Util {
	public static String getClassName() {
		return Thread.currentThread().getStackTrace()[2].getClassName();
	}
	public static String getMethodName() {
		return Thread.currentThread().getStackTrace()[2].getMethodName();
	}
	public static String getNowTime() {
		Calendar cal = Calendar.getInstance();
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
		File file = new File("GAME_log/" + sdf.format(cal.getTime()) + ".log");
		try {
			file.createNewFile();
		} catch (IOException ie){
			ie.printStackTrace();
			System.out.println("棋譜ファイルが作成できませんでした。");
			System.exit(-1);
		}
		
		return sdf.format(cal.getTime());
	}
	
	public static void close(BufferedWriter bw) {											//
		try {																		//
			if (bw != null) {														//
				bw.close();															//
			}																		//
		} catch (IOException ieClose) {												//
			ieClose.printStackTrace();												//
		}																			//
	}																				//
}