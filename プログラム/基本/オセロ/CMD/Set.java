import java.io.IOException;
import java.io.FileReader;
import java.io.BufferedReader;

public class Set {
	private int[][] bord = new int[10][10];
	String[] bordNum = new String[10];
	BufferedReader br;												
	//////////////////////////////////////////////////////////////////
	protected Set(String txt, int[][] bord) {						//ゲーム盤を初期化
		this.bord = bord;											//
																	//
		for(int j = 0; j < 10; j++) {								//再初期化
			for(int k = 0; k < 10; k++) {							//一応ね。一応。
				bord[j][k] = 0;										//
			}														//
		}															//ゲーム盤外の壁の表示
		for(int i = 0; i < 10; i++) {bord[i][0] = 2;}				//↓ここから
		for(int i = 0; i < 10; i++) {bord[i][9] = 2;}				//↓
		for(int i = 0; i < 10; i++) {bord[0][i] = 2;}				//↓
		for(int i = 0; i < 10; i++) {bord[9][i] = 2;}				//↑ここまで
		for(int i = 4; i < 6; i++) {bord[i][i] = -1;}				//初期白石の配置
		bord[4][5] = 1;												//初期黒石の配置
		bord[5][4] = 1;												//初期黒石の配置
																	//
		try {														//
			br = new BufferedReader(new FileReader(txt));			//ゲーム盤マス目行列文字の取得
																	//
			int leftCount = Integer.parseInt(br.readLine());		//
			for (int i = 0; i < leftCount; i++) {					//
				bordNum[i] = br.readLine();							//
			}														//
		} catch (NumberFormatException nfe) {						//
			System.out.println("想定外のエラー");					//txtにエラーが起こってるらしい
			System.out.println(txt + "の中身を確認してください");	//文字化けなどしていないか確認
			System.exit(-1);										//
		} catch (IOException ie) {									//
			System.out.println("Setでエラー : " + ie);				//ファイルがないらしい
			System.exit(-1);										//ダウンロードし直ししてね
		}															//
	}																//
	//////////////////////////////////////////////////////////////////
	protected void bordSet(int[][] bord) {							//現在のゲーム盤を表示
		this.bord = bord;											//
																	//
																	//↓ゲーム盤のマス番号の表示
		System.out.print("　");										//↓ここから//ゲーム盤のマスの行番号の表示
		for(int i = 0; i < 10; i++) {								//↓
			System.out.print(bordNum[i]);							//↓
		}															//↓
		System.out.print("\r\n");									//↓
		for(int j = 0; j < 10; j++) {								//↓
			System.out.print(bordNum[j]);							//↑ここまで//ゲーム盤のマスの列番号の表示
																	//
			for(int k = 0; k < 10; k++) {							//配列の中身によってゲーム盤の表示
				switch (bord[j][k]) {								//
				case 0:												//
					System.out.print("−");							//何も石が置かれていないマスの表示
					break;											//・空マスは0で管理
				case -1 :											//
					System.out.print("●");							//黒石の表示
					break;											//・黒石は配列で-1で管理
				case 1 :											//
					System.out.print("○");							//白石の表示
					break;											//・白石は配列で1で管理
				case 2 :											//
					System.out.print("■");							//ゲーム盤の外側の情報
					break;											//・壁は2で管理
				default :											//
					System.out.println("想定外のエラー");			//起こることはないと思われる。
					System.exit(-1);								//起こったら初期値が失敗している場合など
				}													//
			}														//
																	//
			System.out.print("\r\n");								//
		}															//
	}																//
}/////////////////////////////////////////////////////////////////////
