import java.io.IOException;
import java.io.InputStreamReader;
import java.io.BufferedReader;

public class GameStart {
	String className;													//エラー用
	String methodName = Util.getMethodName();							//エラー用
	
	private int[] first;
	BufferedReader brJun = new BufferedReader(new InputStreamReader(System.in));
	
	protected boolean Jun() {
		className = Util.getClassName();								//エラー用
																		//
		System.out.print("先攻後攻を選択してください(1/0) : ");			//1か0で先攻後攻を入力
		try {															//
			first[0] = Integer.parseInt(brJun.readLine());				//
		} catch (NumberFormatException nfe) {							//数字以外ならエラー
			System.out.println("数字を入力して下さい");					//
			return false;												//
		} catch (IOException ie) {										//これが出たら入力機器のエラー
			System.out.println("入力エラー");							//
			return false;												//
		}																//
		if (first[0] == 0) {											//0なら後攻を表す-1を代入
			first[0] = -1;												//
		}																//
		if(first[0] == 1 || first[0] == -1) {							//1か-1以外なら再入力
			return true;												//
		} else {														//
			System.out.println("1（先攻）か0(後攻)を入力して下さい");	//
			return false;												//
		}																//
	}																	//
	//////////////////////////////////////////////////////////////////////
	protected GameStart(int[] first) {									//
		this.first = first;												//
																		//
		while(!Jun()) {													//先攻後攻入力へ
		}																//入力がおかしければ繰り返し
																		//
		switch (first[0]) {												//先攻後攻判定
		case 1:															//
			System.out.println("貴方は先攻です。");						//"1"なら先攻
			break;														//
		case -1:														//"-1"なら後攻
			System.out.println("貴方は後攻です。");						//先攻情報をfalseに
			break;														//
		default:														//
			System.out.print("想定外のエラー : ");						//想定外
			System.out.println(methodName + className + "を確認");		//エラー処理がうまくいっていない
			System.exit(-1);											//
		}																//
	}																	//
}																		//
