import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.IOException;

public class Osero {
	Set set;													//setをインスタンス化
	Turn turn = new Turn();										//turnをインスタンス化
	String className;											//エラー用
	String methodName = Util.getMethodName();					//エラー用
	private BufferedWriter bwLog;								//
																//
	private int[][] bord = new int[10][10];						//ゲーム盤を定義
	private int turnCount = 1;									//経過ターン数の定義
	private int[] nowTurn = {0};								//ターンプレイヤーの定義
	private boolean[] trunFlag = {true};						//先攻後攻情報
	private String winInfo;										//
	private String nowTime;										//
	//////////////////////////////////////////////////////////////
	private Osero() {											//
		set = new Set("Num.txt", bord);							//Num.txtから初期ゲーム配置情報を取得
	}															//
	//////////////////////////////////////////////////////////////
	private int count(int stoneColor) {							//
		int count = 0;											//
		for(int j = 0; j < 10; j++) {							//引数と同じ石の数をカウント
			for(int k = 0; k < 10; k++) {						//
				if (bord[j][k] == stoneColor) {					//
					count++;									//
				}												//
			}													//
		}														//
		return count;											//
	}															//
	//////////////////////////////////////////////////////////////
	private void game() {										//
		set.bordSet(bord);										//オセロの初期配置を表示
		GameStart gameStart = new GameStart(nowTurn);			//ゲームの開始情報の取得開始
																//
		nowTime = Util.getNowTime();							//開始時時刻を取得
		try {													//
			bwLog = new BufferedWriter(new FileWriter			//
				("GAME_log/" + nowTime + ".log", true));		//
			bwLog.write(nowTime + "開始のゲーム棋譜");			//
			bwLog.newLine();bwLog.newLine();					//
			bwLog.flush();										//
		} catch (IOException ie) {								//
			ie.printStackTrace();								//
			System.out.println("入力エラー");					//
			Util.close(bwLog);									//
		}														//
																//
		while (turn.Turn(bord, nowTurn, turnCount, nowTime)) {	//ここから各プレイヤーのターン進行
			set.bordSet(bord);									//ターン進行後のボード表示
			nowTurn[0] *= -1;									//ターンプレイヤーの変更
			turnCount++;										//ターンの進行カウント
		}														//
																//
		System.out.println("石が置けなくなりました");			//
		System.out.println("ゲームを終了します");				//
		System.out.println("● : " + count(-1));				//
		System.out.println("○ : " + count(1));					//
		if (count(-1) < count(1)) {								//winInfoに勝利情報を代入
			winInfo = "○の勝ちです。";							//
		} else if (count(-1) > count(1)) {						//
			winInfo = "●の勝ちです。";							//
		} else {												//
			winInfo = "同点。引き分けです。";					//
		}														//
		System.out.println(winInfo);							//勝利情報を出力
		try {													//
			bwLog.newLine();									//棋譜の最後に対戦情報を入力
			bwLog.write("ゲーム終了");bwLog.newLine();			//
			bwLog.write("● : " + count(-1));bwLog.newLine();	//
			bwLog.write("○ : " + count(1));bwLog.newLine();	//
			bwLog.write(winInfo);bwLog.newLine();				//
			bwLog.flush();										//
		} catch (IOException ie) {								//
			ie.printStackTrace();								//
			System.out.println("入力エラー");					//
			Util.close(bwLog);									//
		}														//
		System.out.println("ゲームを終了します");				//
		Util.close(bwLog);										//
	}															//
																//
	//////////"オセロゲームメインメソッド"////////////////////////
	public static void main(String[] args) {					//
		Osero osero = new Osero();								//オセロのゲーム盤のインスタンス化
																//
		osero.game();											//オセロゲーム開始
	}															//
}																//
